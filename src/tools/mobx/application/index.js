import { observable, action } from 'mobx'
import { ModuleFoodGet } from '../../../modules/food/get'
import { ModuleFoodAdd } from '../../../modules/food/add'
import { ModuleFoodDelete } from '../../../modules/food/delete'
import { ModuleMenu } from '../../../modules/menu'
import { ModuleFoodPick } from '../../../modules/food/pick'

import { API_RING } from '../../../config'

class Application {
  constructor (RootStore) {
    this.RootStore = RootStore
  }

  @observable isSelected = 0
  @observable dataSource = []
  @observable isPublic = false
  @observable isLock = false
  @observable dataSourceSchedule = []
  @observable dataSourcePick = []
  @observable idUpdatePick = ``
  @observable dataPick = {}

  @action loadFood = async () => {
    let T = await ModuleFoodGet.whereToDay()
    T.onSnapshot(snap => {
      let L = []
      snap.forEach(doc => {
        L.push({
          ID: doc.id,
          key: doc.id,
          ...doc.data()
        })
      })
      this.dataSource = L
    })
  }

  @action addFood = async ({ten, soluong}) => {
    await ModuleFoodAdd.addToDay(ten, soluong)
  }

  @action deleteFood = async (id) => {
    await ModuleFoodDelete.deleteToDay(id)
  }

  @action onPublic = async (isPublic) => {
    if (isPublic) {
      this.isPublic = true
      let l = window.open(API_RING)
      setTimeout(() => {
        l.close()
      }, 500)
      setTimeout(() => {
        ModuleMenu.onMenuPublic(isPublic)
      }, 15000)
    } else await ModuleMenu.onMenuPublic(isPublic)
  }

  @action onLock = async (onLock) => {
    await ModuleMenu.onMenuLock(onLock)
  }
  
  @action loadGetMenu = async () => {
    let T = await ModuleMenu.getMenu()
    T.onSnapshot(doc => {
      this.isPublic = doc.data().isPublic
      this.isLock = doc.data().isLock
    })
  }

  @action loadGetSchedule = async () => {
    let T = await ModuleFoodGet.orderToDay()
    T.onSnapshot(snap => {
      let L = []
      this.dataSource.forEach(v => {
        let SL = 0
        let GHICHU = ''
        snap.forEach(doc => {
          if (v.ten === doc.data().monan) {
            SL += doc.data().soluong
            if (doc.data().ghichu !== '') {
              GHICHU += `${doc.data().nguoidat}: ${doc.data().ghichu}|`
            }
          }
        })
        L.push({
          ID: v.ID,
          key: v.key,
          ten: v.ten,
          soluong: SL,
          ghichu: GHICHU
        })
      })
      this.dataSourceSchedule = L
    })
  }

  @action loadGetPick = async () => {
    let T = await ModuleFoodGet.pickToDay()
    T.onSnapshot(snap => {
      let L = []
      let L_Schedule = [...this.dataSourceSchedule]
      let L_Food = [...this.dataSource]
      // snap.forEach(doc => console.log(doc.data()))
      L_Schedule.forEach((v, i) => {
        let K = 0
        snap.forEach(doc => {
          if (v.ten === doc.data().monan) {
            L.push({
              ID: v.ID,
              key: v.key,
              ten: v.ten,
              soluong: `${v.soluong}/${L_Food[i].soluong}`,
              conlai: +L_Food[i].soluong - +v.soluong,
              dadat: doc.data().soluong,
              thaotac: `${doc.id}|${+L_Food[i].soluong - +v.soluong}`
            })
            K++
          }
        })
        if (K === 0) {
          L.push({
            ID: v.ID,
            key: v.key,
            ten: v.ten,
            soluong: `${v.soluong}/${L_Food[i].soluong}`,
            conlai: +L_Food[i].soluong - +v.soluong,
            dadat: 0,
            thaotac: `${null}|${+L_Food[i].soluong - +v.soluong}|${v.ten}`
          })
        }
      })
      this.dataSourcePick = L
    })
  }

  @action addPick = async (monan, soluong, ghichu) => {
    ModuleFoodPick.pickToDay(monan, soluong, ghichu)
  }

  @action updatePick = async (id, soluong, ghichu) => {
    ModuleFoodPick.updateToDay(id, soluong, ghichu)
  }

  @action deletePick = async (id) => {
    ModuleFoodPick.deleteToDay(id)
  }

  @action getByIDPick = async (id) => {
    let T = await ModuleFoodPick.getPickByID(id)
    this.dataPick = T
  }

  @action setByIDPick = async (id) => {
    this.idUpdatePick = id
  }
}

export { Application }
