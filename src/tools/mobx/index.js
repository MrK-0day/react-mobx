import { Authen } from './authen'
import { Application } from './application'

export class RootStore {
  constructor () {
    this.Authen = new Authen(this)
    this.Application = new Application(this)
  }
}