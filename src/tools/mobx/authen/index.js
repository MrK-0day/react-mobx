import { observable, action } from 'mobx'
import { USER_ADMIN } from '../../../config'
import { ModuleUserLogin } from '../../../modules/user/login'
import { Notification } from '../../../components/notification'

class Authen {
  constructor (RootStore) {
    this.RootStore = RootStore
  }

  @observable isLoading = false
  @observable isLogin = false
  @observable isAdmin = false
  @observable dataUser = null

  @action onLogin = async (password) => {
    if (typeof password === 'object' && password === null) {
      // Login With FB
      let T = await ModuleUserLogin.facebook()
      if (T.hasOwnProperty('user')) {
        this.dataUser = T.user
        this.isLogin = true
        this.isAdmin = false
      } else {
        Notification('error', T.message)
      }
    } else {
      // Login With Password
      let T = await ModuleUserLogin.password(password)
      if (T.hasOwnProperty('user')) {
        this.dataUser = T.user
        this.isLogin = true
        this.isAdmin = true
      } else {
        Notification('error', T.message)
      }
    }
  }

  @action checkLogin = async () => {
    let T = await ModuleUserLogin.check()
    if (!!T) {
      this.dataUser = T
      this.isLogin = true
      if (T.email === USER_ADMIN) this.isAdmin = true
      else this.isAdmin = false
    } else {
      this.isLogin = false
    }
  }

  @action onLoading = async (delay) => {
    this.isLoading = true
    setTimeout(() => {
      this.isLoading = false
    }, delay)
  }

  @action onLogout = async () => {
    await ModuleUserLogin.logout()
    this.isLogin = false
    this.isAdmin = false
    this.dataUser = null
  }
}

export { Authen }
