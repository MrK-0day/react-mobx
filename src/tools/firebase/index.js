import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

const config = {
  apiKey: "AIzaSyBDtUV9Q3jlb1G6o-7VCy_0Fcgd5mKpL90",
  authDomain: "innos-lunch.firebaseapp.com",
  databaseURL: "https://innos-lunch.firebaseio.com",
  projectId: "innos-lunch",
  storageBucket: "innos-lunch.appspot.com",
  messagingSenderId: "327504018765"
}

firebase.initializeApp(config)
const db = firebase.firestore()
db.settings({
  timestampsInSnapshots: true
})

export { firebase, db }
