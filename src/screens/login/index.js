import React from 'react'
import { observer, inject } from 'mobx-react'

import { LOGIN_CONFIG, HEADER_CONFIG } from '../../config'

import LoginFormUser from '../../components/login/form/user'
import LoginFormAdmin from '../../components/login/form/admin'
import Loading from '../../components/loading'

import './index.scss'

@inject((allStore) => ({
  Authen: allStore.RootStore.Authen
}))
@observer
class ScreensLogin extends React.Component {
  componentDidMount () {
    this.props.Authen.onLoading(1000)
  }
  render () {
    const { Authen } = this.props
    return (
      <React.Fragment>
        {Authen.isLoading ? (<Loading />) : (
          <div className='login animated fadeIn faster'>
            <div className='box'>
              <img className='logo' alt={HEADER_CONFIG.TITLE} src={LOGIN_CONFIG.LOGO} />
              <div className='form'>
                <LoginFormUser onLogin={Authen.onLogin} />
                <LoginFormAdmin onLogin={Authen.onLogin} />
              </div>
            </div>
          </div>
        )}
      </React.Fragment>
    )
  }
}

export default ScreensLogin
