import React from 'react'
import { observer, inject } from 'mobx-react'
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import Loadable from 'react-loadable'

import { PATH_CONFIG } from '../config'

import { Header } from '../components/header'

import 'antd/dist/antd.css'
import '../assets/css/animate.css'
import '../assets/css/theme.scss'

const LoadingScreensLogin = Loadable({
  loader: () => import('./login'),
  loading: () => null
})
const LoadingScreensApplication = Loadable({
  loader: () => import('./application'),
  loading: () => null
})

@inject((allStore) => ({
  Authen: allStore.RootStore.Authen
}))
@observer
class ScreensRouter extends React.Component {
  componentWillMount () {
    this.props.Authen.checkLogin()
  }
  render () {
    const { isLogin } = this.props.Authen
    return (
      <BrowserRouter>
        <React.Fragment>
          <Header />
          <Switch>
            <Route
              exact={PATH_CONFIG.LOGIN.exact}
              path={PATH_CONFIG.LOGIN.path}
              render={(props) => {
                return isLogin ? (
                  <Redirect to={PATH_CONFIG.APPLICATION.path} />
                ) : (
                  <LoadingScreensLogin
                    {...props}
                  />
                )
              }}
            />
            <Route
              exact={PATH_CONFIG.APPLICATION.exact}
              path={PATH_CONFIG.APPLICATION.path}
              render={(props) => {
                return isLogin ? (
                  <LoadingScreensApplication
                    {...props}
                  />
                ) : (
                  <Redirect to={PATH_CONFIG.LOGIN.path} />
                )
              }}
            />
            <Redirect to={PATH_CONFIG.LOGIN.path} />
          </Switch>
        </React.Fragment>
      </BrowserRouter>
    )
  }
}

export default ScreensRouter
