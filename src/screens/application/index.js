import React from 'react'
import { observer, inject } from 'mobx-react'

import LayoutG from '../../components/layout'
import Loading from '../../components/loading'
import FoodCreate from '../../components/food/create'
import FoodSchedule from '../../components/food/schedule'
import FoodPick from '../../components/food/pick'

import './index.scss'

@inject((allStore) => ({
  Authen: allStore.RootStore.Authen,
  Application: allStore.RootStore.Application
}))
@observer
class ScreensApplication extends React.Component {
  componentDidMount () {
    this.props.Authen.onLoading(1000)
    this.props.Application.loadFood()
    this.props.Application.loadGetMenu()
    this.props.Application.loadGetSchedule()
    this.props.Application.loadGetPick()
  }
  render () {
    const { Authen } = this.props
    const { Application } = this.props
    const MENU = [
      {
        title: `Tạo thực đơn`,
        icon: `menu-unfold`
      },
      {
        title: `Đặt cơm`,
        icon: `select`
      },
      {
        title: `Thống kê`,
        icon: `dashboard`
      }
    ]
    const MENU1 = [
      {
        title: `Đặt cơm`,
        icon: `select`
      }
    ]
    return (
      <React.Fragment>
        {Authen.isLoading ? (<Loading />) : (
          <div className='application animated fadeIn faster'>
            <LayoutG
              onLogout={Authen.onLogout}
              menu={Authen.isAdmin ? MENU : MENU1}
              onSelect={(v) => Application.isSelected = +v}
            >
              {Authen.isAdmin && Application.isSelected === 0 && (
                <FoodCreate
                  dataSource={Application.dataSource}
                  addFood={Application.addFood}
                  deleteFood={Application.deleteFood}
                  onPublic={Application.onPublic}
                  onLock={Application.onLock}
                  isPublic={Application.isPublic}
                  isLock={Application.isLock}
                />
              )}
              {Authen.isAdmin ? (
                <React.Fragment>
                  {Application.isSelected === 1 && (
                    <FoodPick
                      isPublic={Application.isPublic}
                      dataSource={Application.dataSourcePick}
                      isLock={Application.isLock}
                      addPick={Application.addPick}
                      deletePick={Application.deletePick}
                      updatePick={Application.updatePick}
                      getByIDPick={Application.getByIDPick}
                      setByIDPick={Application.setByIDPick}
                      idUpdatePick={Application.idUpdatePick}
                      dataPick={Application.dataPick}
                    />
                  )}
                </React.Fragment>
              ) : (
                <React.Fragment>
                  {Application.isSelected === 0 && (
                    <FoodPick
                      isPublic={Application.isPublic}
                      dataSource={Application.dataSourcePick}
                      isLock={Application.isLock}
                      addPick={Application.addPick}
                      deletePick={Application.deletePick}
                      updatePick={Application.updatePick}
                      getByIDPick={Application.getByIDPick}
                      setByIDPick={Application.setByIDPick}
                      idUpdatePick={Application.idUpdatePick}
                      dataPick={Application.dataPick}
                    />
                  )}
                </React.Fragment>
              )}
              {Authen.isAdmin && Application.isSelected === 2 && (
                <FoodSchedule
                  isPublic={Application.isPublic}
                  dataSource={Application.dataSourceSchedule}
                />
              )}
            </LayoutG>
          </div>
        )}
      </React.Fragment>
    )
  }
}

export default ScreensApplication
