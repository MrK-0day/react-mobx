export const PATH_CONFIG = {
  APPLICATION: {
    path: '/🚀',
    exact: false
  },
  LOGIN: {
    path: '/🔑',
    exact: true
  }
}

export const HEADER_CONFIG = {
  TITLE: 'INNOS Lunch',
  FOTN: 'https://fonts.googleapis.com/css?family=Open+Sans',
  ICON: require('../assets/images/favicon.png')
}

export const LOGIN_CONFIG = {
  LOGO: require('../assets/images/innos.png')
}

export const USER_ADMIN = `admin@innos.lunch`

export const API_RING = `http://192.168.1.200:7001/`
