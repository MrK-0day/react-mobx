import React from 'react'

import { Layout, Menu, Icon } from 'antd'

import { HEADER_CONFIG, LOGIN_CONFIG } from '../../config'

class LayoutG extends React.PureComponent {
  render () {
    return (
      <Layout style={{ width: `100%`, height: `100%` }}>
        <Layout.Sider
          collapsible
          theme='light'
          className='sider'
        >
          <div style={{
            textAlign: 'center',
            padding: 10,
            borderRight: `1px solid #e8e8e8`
          }}>
            <img style={{ width: '90%' }} alt={HEADER_CONFIG.TITLE} src={LOGIN_CONFIG.LOGO} />
          </div>
          <Menu
            defaultSelectedKeys={['0']}
            mode='inline'
            theme='light'
            onSelect={(e) => this.props.onSelect(e.key)}
          >
            {this.props.menu.map((v, i) => {
              return (
                <Menu.Item key={i}>
                  <Icon type={v.icon} />
                  <span>{v.title}</span>
                </Menu.Item>
              )
            })}
          </Menu>
        </Layout.Sider>
        <Layout>
          <Layout.Header className='header'>
            <div
              style={{
                float: 'right',
                width: 'auto',
                marginLeft: 20,
                marginRight: 20
              }}
              onClick={this.props.onLogout}
            >
              <Icon className='trigger' type='logout' />
            </div>
          </Layout.Header>
          <Layout.Content className='content'>
            {this.props.children}
          </Layout.Content>
        </Layout>
      </Layout>
    )
  }
}

export default LayoutG
