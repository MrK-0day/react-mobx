import React from 'react'

import { Table, Button } from 'antd'

import FoodPickModal from './modal'
class FoodPick extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      ten: ``,
      conlai: -1,
      isDatCom: false
    }
  }
  render () {
    const COLUMNS = [
      {
        title: 'Tên món ăn',
        dataIndex: 'ten',
        key: 'ten'
      },
      {
        title: 'Số lượng',
        dataIndex: 'soluong',
        key: 'soluong'
      },
      {
        title: 'Đã đặt',
        dataIndex: 'dadat',
        key: 'dadat',
        render: (v) => {
          return <span>{v}</span>
        }
      },
      {
        title: `Thao tác`,
        dataIndex: 'thaotac',
        key: 'thaotac',
        render: (v) => {
          // console.log(v)
          let data = v.split('|')
          return (
            <React.Fragment>
              {this.props.isLock ? (
                <p>Hệ thống đã bị khóa.</p>
              ) : (
                <React.Fragment>
                  {data[0] === 'null' ? (
                    <React.Fragment>
                      {+data[1] === 0 ? (
                        <p>Hết cơm rồi nha !!!</p>
                      ) : (
                        <Button
                          onClick={() => {
                            this.setState({
                              visible: true,
                              isDatCom: true,
                              ten: data[2],
                              conlai: data[1]
                            })
                          }}
                          type='primary'
                        >
                          Đặt cơm
                        </Button>
                      )}
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <Button
                        type='danger'
                        style={{ marginRight: 5 }}
                        onClick={() => this.props.deletePick(data[0])}
                      >
                        Hủy
                      </Button>
                      {/* <Button
                        type='default'
                        onClick={() => {
                          this.setState({
                            visible: true,
                            isDatCom: false,
                            conlai: data[1],
                            ten: ``
                          })
                          this.props.setByIDPick(data[0])
                          this.props.getByIDPick(data[0])
                        }}
                      >
                        Thay đổi
                      </Button> */}
                    </React.Fragment>
                  )}
                </React.Fragment>
              )}
            </React.Fragment>
          )
        }
      }
    ]
    return (
      <React.Fragment>
        {this.props.isPublic ? (
          <React.Fragment>
            <Table columns={COLUMNS} dataSource={this.props.dataSource} />
            <FoodPickModal
              visible={this.state.visible}
              onCancel={() => this.setState({
                visible: false,
                ten: ``,
                conlai: -1,
                isDatCom: false
              })}
              ten={this.state.ten}
              conlai={this.state.conlai}
              isDatCom={this.state.isDatCom}
              idUpdatePick={this.props.idUpdatePick}
              dataPick={this.props.dataPick}
              getByIDPick={(id) => this.props.getByIDPick(id)}
              onUpdateDatCom={(id, soluong, ghichu) => this.props.updatePick(id, soluong, ghichu)}
              onDatCom={(monan, soluong, ghichu) => this.props.addPick(monan, soluong, ghichu)}
            />
          </React.Fragment>
        ) : <h3>Vui lòng đợi trong giây lát.</h3>}
      </React.Fragment>
    )
  }
}

export default FoodPick
