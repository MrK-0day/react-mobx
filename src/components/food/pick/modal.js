import React from 'react'

import { Modal, Input } from 'antd'

class FoodPickModal extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      soluong: 1,
      ghichu: ''
    }
  }
  render () {
    if (this.props.isDatCom) {
      return (
        <Modal
          title={`Đặt cơm`}
          visible={this.props.visible}
          onOk={() => {
            this.props.onDatCom(this.props.ten, this.state.soluong, this.state.ghichu)
            this.setState({ soluong: 1, ghichu: '' })
            this.props.onCancel()
          }}
          onCancel={() => {
            this.setState({ soluong: 1, ghichu: '' })
            this.props.onCancel()
          }}
          cancelText={`Hủy`}
          okText={`Đặt cơm`}
        >
          <p>Món ăn : {this.props.ten}</p>
          <Input onChange={(e) => {
            if (+e.target.value <= +this.props.conlai && +e.target.value >= 1) {
              this.setState({
                soluong: +e.target.value
              })
            }
          }} type='number' placeholder='SL' value={this.state.soluong} />
          <hr style={{ border: 'none' }} />
          <Input.TextArea onChange={(e) => {
            this.setState({ ghichu: e.target.value })
          }} placeholder='Ghi chú' value={this.state.ghichu} />
        </Modal>
      )
    } else {
      return (
        <React.Fragment>
          {this.props.dataPick.monan !== undefined && (
            <Modal
              title={`Thay đổi`}
              visible={this.props.visible}
              onOk={() => {
                // this.props.onDatCom(this.props.ten, this.state.soluong, this.state.ghichu)
                this.setState({ soluong: 1, ghichu: '' })
                this.props.onCancel()
              }}
              onCancel={() => {
                this.setState({ soluong: 1, ghichu: '' })
                this.props.onCancel()
              }}
              cancelText={`Hủy`}
              okText={`thay đổi`}
            >
              <p>Món ăn : {this.props.dataPick.monan}</p>
              <Input onChange={(e) => {
                if (+e.target.value <= +this.props.conlai && +e.target.value >= 1) {
                  this.setState({
                    soluong: +e.target.value
                  })
                }
              }} type='number' placeholder='SL' value={this.state.soluong} />
              <hr style={{ border: 'none' }} />
              <Input.TextArea onChange={(e) => {
                this.setState({ ghichu: e.target.value })
              }} placeholder='Ghi chú' value={this.state.ghichu} />
            </Modal>
          )}
        </React.Fragment>
      )
    }
  }
}

export default FoodPickModal
