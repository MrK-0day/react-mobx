import React from 'react'

import { Input, Button, Table } from 'antd'

class FoodCreate extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      ten: '',
      soluong: 0
    }
    this.onAdd = this.onAdd.bind(this)
  }
  onAdd () {
    this.props.addFood(this.state)
    this.setState({ ten: '', soluong: 0 })
  }
  render () {
    const COLUMNS = [
      {
        title: 'Tên món ăn',
        dataIndex: 'ten',
        key: 'ten'
      },
      {
        title: 'Số lượng',
        dataIndex: 'soluong',
        key: 'soluong'
      },
      {
        title: 'Thao tác',
        dataIndex: 'ID',
        key: 'ID',
        render: id => {
          return this.props.isPublic ? null : <Button onClick={() => this.props.deleteFood(id)} type='danger'>Xóa</Button>
        }
      }
    ]
    return (
      <React.Fragment>
        <div style={{ width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', marginBottom: 20 }}>
          <div style={{ width: '70%' }} />
          <div style={{ width: '30%' }}>
            <Button
              style={{ float: 'right', marginLeft: 10 }}
              type={!this.props.isLock ? `default` : `primary`}
              onClick={() => {
                this.props.onLock(!this.props.isLock)
              }}
            >
              {!this.props.isLock ? `Khóa` : `Mở khóa`}
            </Button>
            {!this.props.isLock && (
              <Button
                style={{ float: 'right' }}
                type={!this.props.isPublic ? `primary` : `danger`}
                onClick={() => {
                  this.props.onPublic(!this.props.isPublic)
                }}
              >
                {!this.props.isPublic ? `Xuất thực đơn` : `Thu hồi thực đơn`}
              </Button>
            )}
          </div>
        </div>
        <Input.Group style={{ marginBottom: 20 }} compact>
          <Input
            placeholder='Món ăn'
            size='large'
            style={{ width: '73%' }}
            value={this.state.ten}
            onChange={(e) => this.setState({ ten: e.target.value })}
            onPressEnter={this.onAdd}
            tabIndex='1'
            autoFocus
            disabled={this.props.isPublic}
          />
          <Input
            placeholder='SL'
            style={{ width: '7%' }}
            type='number'
            size='large'
            value={this.state.soluong}
            onChange={(e) => {
              if (+e.target.value >= 0) {
                this.setState({ soluong: +e.target.value })
              }
            }}
            onPressEnter={this.onAdd}
            tabIndex='2'
            disabled={this.props.isPublic}
          />
          <Button
            style={{ width: '20%' }}
            size='large'
            type='primary'
            onClick={this.onAdd}
            tabIndex='3'
            disabled={this.props.isPublic}
          >
            Thêm vào thực đơn
          </Button>
        </Input.Group>
        <Table columns={COLUMNS} dataSource={this.props.dataSource} />
      </React.Fragment>
    )
  }
}

export default FoodCreate
