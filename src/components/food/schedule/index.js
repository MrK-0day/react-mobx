import React from 'react'

import { Table } from 'antd'

class FoodSchedule extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }
  render () {
    const COLUMNS = [
      {
        title: 'Tên món ăn',
        dataIndex: 'ten',
        key: 'ten'
      },
      {
        title: 'Số lượng',
        dataIndex: 'soluong',
        key: 'soluong'
      },
      {
        title: 'Ghi chú',
        dataIndex: 'ghichu',
        key: 'ghichu',
        render: data => {
          let D = data.split('|')
          let X = D.map((v, i) => {
            return (
              <p key={i}>{v}</p>
            )
          })
          return X
        }
      }
    ]
    return (
      <React.Fragment>
        {this.props.isPublic ? (
          <React.Fragment>
            <Table columns={COLUMNS} dataSource={this.props.dataSource} />
          </React.Fragment>
        ) : <h3>Vui lòng xuất thực đơn để xem thống kê.</h3>}
      </React.Fragment>
    )
  }
}

export default FoodSchedule
