import React from 'react'
import { Helmet } from 'react-helmet'

import { HEADER_CONFIG } from '../../config'

export const Header = () => {
  return (
    <Helmet>
      <title>{HEADER_CONFIG.TITLE}</title>
      <link href={HEADER_CONFIG.FOTN} rel='stylesheet' />
      <link rel='shortcut icon' href={HEADER_CONFIG.ICON} />
    </Helmet>
  )
}
