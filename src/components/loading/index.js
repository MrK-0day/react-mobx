import React from 'react'
import { withRouter } from 'react-router-dom'
import { BarLoader } from 'react-spinners'

import './index.scss'

class Loading extends React.PureComponent {
  render () {
    return (
      <React.Fragment>
        <div className='loading'>
          <div className='box'>
            <BarLoader
              width={100}
              widthUnit={`vw`}
              height={4}
              heightUnit={`px`}
              loading={!!true}
              color={`#1890ff`}
            />
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default withRouter(Loading)
