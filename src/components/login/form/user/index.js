import React from 'react'
import { Divider, Button, Icon } from 'antd'

const LoginFormUser = ({ onLogin }) => {
  return (
    <React.Fragment>
      <Divider>Nhân Viên</Divider>
      <Button onClick={() => onLogin(null)} style={{ width: '100%' }} type='primary'>
        <Icon type='facebook' /> Đăng nhập với Facebook
      </Button>
    </React.Fragment>
  )
}

export default LoginFormUser
