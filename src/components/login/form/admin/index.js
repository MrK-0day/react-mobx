import React from 'react'

import { Divider, Form, Input, Button, Icon } from 'antd'

class LoginFormAdmin extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {}
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  hasErrors (fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field])
  }
  handleSubmit (e) {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onLogin(values.password)
      }
    })
  }
  render () {
    const { getFieldDecorator, getFieldsError } = this.props.form
    return (
      <React.Fragment>
        <Divider>Quản Lý</Divider>
        <Form onSubmit={this.handleSubmit}>
          <Form.Item hasFeedback>
            {getFieldDecorator('password', {
              rules: [{
                required: true,
                message: 'Vui lòng nhập mật khẩu !',
                validator: (rule, value, callback) => {
                  value === '' ? callback(new Error(true)) : callback()
                }
              }]
            })(
              <Input
                prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} type='password' placeholder='Mật khẩu'
              />
            )}
          </Form.Item>
          <Form.Item>
            <Button disabled={this.hasErrors(getFieldsError())} htmlType='submit' style={{ width: '100%' }} type='primary'>
              <Icon type='login' /> Đăng nhập
            </Button>
          </Form.Item>
        </Form>
      </React.Fragment>
    )
  }
}

export default Form.create()(LoginFormAdmin)
