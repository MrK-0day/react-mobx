import { firebase } from '../../../tools/firebase'
import { USER_ADMIN } from '../../../config'

const SHA512 = require('crypto-js/sha512')

export const ModuleUserLogin = {
  facebook: async () => {
    let provider = new firebase.auth.FacebookAuthProvider()
    provider.setCustomParameters({ display: `popup` })
    let T = await new Promise((resolve, reject) => {
      firebase.auth().signInWithPopup(provider).then(U => resolve(U)).catch(e => resolve(e))
    })
    return T
  },
  password: async (password) => {
    let HASH = SHA512(password).toString()
    let T = await new Promise((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(USER_ADMIN, HASH).then(U => resolve(U)).catch(e => resolve(e))
    })
    return T
  },
  check: async () => {
    let T = await new Promise((resolve, reject) => {
      firebase.auth().onAuthStateChanged(U => {
        if (U) resolve(U)
        resolve(null)
      })
    })
    return T
  },
  logout: async () => {
    let T = await firebase.auth().signOut()
    return T
  }
}
