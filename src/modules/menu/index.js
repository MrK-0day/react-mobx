import { db } from '../../tools/firebase'

export const ModuleMenu = {
  onMenuPublic: async (isPublic) => {
    let T = await new Promise(resolve => {
      db.collection('admin').doc('config').update({
        isPublic
      }).then(() => resolve(true)).catch(() => resolve(false))
    })
    return T
  },
  onMenuLock: async (isLock) => {
    let T = await new Promise(resolve => {
      db.collection('admin').doc('config').update({
        isLock
      }).then(() => resolve(true)).catch(() => resolve(false))
    })
    return T
  },
  getMenu: async () => {
    return db.collection('admin').doc('config')
  }
}
