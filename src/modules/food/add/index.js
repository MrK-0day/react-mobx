import { db } from '../../../tools/firebase'
import moment from 'moment'

export const ModuleFoodAdd = {
  addToDay: async (ten, soluong) => {
    let T = await new Promise((resolve, reject) => {
      db.collection('monan').add({
        ten, soluong, ngay: moment().format('DD/MM/YYYY')
      }).then(docRef => resolve(docRef)).catch(e => resolve(e))
    })
    return T
  }
}
