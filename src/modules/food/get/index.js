import { firebase, db } from '../../../tools/firebase'
import moment from 'moment'

export const ModuleFoodGet = {
  whereToDay: async () => {
    return db.collection('monan').where('ngay', '==', moment().format('DD/MM/YYYY'))
  },
  orderToDay: async () => {
    return db.collection('datcom').where('ngay', '==', moment().format('DD/MM/YYYY'))
  },
  pickToDay: async () => {
    let T = await new Promise((resolve, reject) => {
      firebase.auth().onAuthStateChanged(U => resolve(U))
    })
    let U = T.displayName === null ? 'Admin' : T.displayName
    return db.collection('datcom').where('ngay', '==', moment().format('DD/MM/YYYY')).where('nguoidat', '==', U)
  }
}
