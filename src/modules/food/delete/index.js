import { db } from '../../../tools/firebase'
// import moment from 'moment'

export const ModuleFoodDelete = {
  deleteToDay: async (id) => {
    let T = await new Promise(resolve => {
      db.collection('monan').doc(id).delete().then(() => resolve(true)).catch(() => resolve(false))
    })
    return T
  }
}
