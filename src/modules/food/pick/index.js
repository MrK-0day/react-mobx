import { firebase, db } from '../../../tools/firebase'
import moment from 'moment'

export const ModuleFoodPick = {
  pickToDay: async (monan, soluong, ghichu) => {
    let T = await new Promise((resolve, reject) => {
      firebase.auth().onAuthStateChanged(U => resolve(U))
    })
    let U = T.displayName === null ? 'Admin' : T.displayName
    let K = await new Promise((resolve, reject) => {
      db.collection('datcom').add({
        ghichu, soluong, ngay: moment().format('DD/MM/YYYY'), monan, nguoidat: U
      }).then(docRef => resolve(docRef)).catch(e => resolve(e))
    })
    return K
  },
  deleteToDay: async (id) => {
    let T = await new Promise(resolve => {
      db.collection('datcom').doc(id).delete().then(() => resolve(true)).catch(() => resolve(false))
    })
    return T
  },
  updateToDay: async (id, soluong, ghichu) => {
    let T = await new Promise(resolve => {
      db.collection('datcom').doc(id).update({
        soluong, ghichu
      }).then(() => resolve(true)).catch(() => resolve(false))
    })
    return T
  },
  getPickByID: async (id) => {
    let T = await new Promise(resolve => {
      db.collection('datcom').doc(id).get().then((v) => resolve(v.data())).catch(() => resolve(false))
    })
    return T
  }
}
