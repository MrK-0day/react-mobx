# Codebase React

## 1. Assets
1. css
- Chứa css dùng chung cho toàn bộ chương trình.
- "variable.scss" => khai báo biến trong scss.
- "theme.scss" => scss dùng chung.
  
2. images
- Chứa hình ảnh dùng chung cho toàn bộ chương trình.

## 2. Config
- Chứa các cài đặt cần thiết (title, icon, path for router).
- Khai báo cú pháp
  ```javascript
  export const [name] = {
    [key]: [value]
  }
  ```

## 3. Components
- Chứa components làm nhiệm vụ render và logic.
- Trong thư mục này, ta group files theo module/tính năng.
  ```
  src
  └─ components
    └─ user
      ├─ form.js
      ├─ list.js
  ```
- Khi component là kết hợp của nhiều component lại ta gom các file components vào 1 thư mục. Thí dụ nếu có thêm file Form.css ta sẽ làm như sau.
  ```
  src
  └─ components
    └─ user
      ├─ form
      │ ├─ form.js
      │ └─ form.scss
      └─ list.js
  ```
- Bên trong thư mục components ta có thể có thêm thư mục UI, trong đây sẽ chứa các component dạng generic (component dạng giống như những React UI Components của Sematic UI, Office Fabric UI, Reactrap,...).
  ```
  src
  └─ components
    └─ UI
  ```
- Đặt tên cho component, ta đặt tên theo quy tắc sau, ví dụ: component ở file [components/user/list.js] thì đặt là [UserList].
- Nếu [components/user/form/form.js] thì không cần đặt là [UserFormForm], chỉ cần gọi nó là [UserForm].

## 4. Screens
- Chứa những components được map vào cho route, như screen để tạo user mới, reset password, đăng nhập.
  ```
  src
  └─ screens
    └─ user
      ├─ register.js
      └─ login.js
  ```
  ```javascript
  <Router>
    <Switch>
      <Route path="/user/login" component={ScreensUserLogin} />
      <Route path="/user/register" component={ScreensUserRegister} />
    </Switch>
  </Router>
  ```
## 5. Modules
- Chứa các module của chương trình, gồm các module như (đăng nhập, đăng xuất,...).
  ```
  src
  └─ modules
    └─ user
      ├─ login
      │ └─ index.js
      ├─ register
      │ └─ index.js
      └─ index.js
  ```

## 6. Tools
- Chứa các gói sử dụng bên thứ 3 như (mobx, redux, firebase,...).
  ```
  src
  └─ tools
    ├─ redux
    │ ├─ reducers
    │ │ └─ authen
    │ │   └─ index.js
    │ ├─ saga
    │ │ └─ authen
    │ │   └─ index.js
    │ ├─ types
    │ │ └─ authen.js
    │ └─ index.js
    ├─ firebase
    │ └─ index.js
    └─ mobx
      └─ authen
        └─ index.js
  ```
